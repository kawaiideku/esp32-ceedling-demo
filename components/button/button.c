#include "driver/gpio.h"
#include "button.h"

#include "esp_log.h"

static const char TAG[] = "button";

button_err_t button_init(gpio_num_t butt_gpio)
{
  gpio_config_t cfg = {
    .pin_bit_mask = 1ULL << butt_gpio,
    .mode = GPIO_MODE_INPUT,
    .intr_type = GPIO_INTR_NEGEDGE
  };
  
  if (ESP_OK != gpio_config(&cfg)) {
    ESP_LOGE(TAG, "%s() - gpio_config() error", __FUNCTION__);
    return Button_error;
  }
  
  ESP_LOGI(TAG, "%s() OK", __FUNCTION__);
  return Button_ok;
}
