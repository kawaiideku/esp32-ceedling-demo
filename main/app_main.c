#include "sdkconfig.h"
#include "button.h"

#include "esp_log.h"

static const char TAG[] = "app_main";

void app_main(void)
{
  ESP_LOGI(TAG, "Hello from %s()!", __FUNCTION__);
  button_init(CONFIG_BUTTON_GPIO);
}
